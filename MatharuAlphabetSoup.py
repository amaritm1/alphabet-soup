# Need to import to take command line arguments
import sys

# Opens the text file passed from the command line
wordsearch = open(sys.argv[1])

'''
I like cats. No other reason for variable name.
Reads the text file and stores it into the cat variable.
'''
cat = wordsearch.readlines()

# Assuming correct fomrat will split the given dimensions
dim = cat[0].split('x')

# stores height of wordsearch
height = int(dim[0])

# stores width of wordsearch
width = int(dim[1])

# stores the wordsearch in a grid
grid = cat[1:height + 1]

# stores the words we need to find
words = cat[(height + 1):len(cat)]

# stors how many words we have to search
wordcount = len(words)

# a temporary iterator to avoid scope issues
tempiter = 0

# List that will store all the first letters of each word
firstletters = []

# List that will store all the last letters of each word
lastletters = []

# This will store the information that makes a word findable in the search
Words = []

# a temporary list because it made it Words easier to make
templist = []

# a mutable boolean variable
Flag = True

'''
All the functions below have the same feeling so I will make one comment
about summarizing them.

inputs: Each fucntion takes in inputs like func(int, int, list,bool)

iter1 and iter2 will serve as the starting point on the gird

Word is the word we are searching for and tempiter is a local iterator passed
through to avoid scoping issues.

Flag is just a booleon used to stop checking a direction and to print
the desired output if the word is found

Each functions checks to see if the location of the last letter of the
word we are searching for is well defined relative to the length of the word,
starting location on the board, and search direction.
Given that location of the last letter is well defined we
then check in the desired direction if the consecutive
letters mach up. if they do not the flag changes and we exit the loop if we
are successful we print out the word and the location of the first letter and
location of the last letter
'''

def DownRight(iter1, iter2, Word, Flag):
    if 0 <= iter1 + Word[3] < height and 0 <= iter2 + Word[3] < width:
        if grid[iter1 + Word[3]][iter2 + Word[3]] == Word[2]:
            tempiter = 1
            while tempiter < Word[3] and Flag == True:
                if grid[iter1 + tempiter][iter2 + tempiter] == Word[0][
                    tempiter]:
                    tempiter = tempiter + 1
                else:
                    Flag = False
            if Flag == True:
                print(Word[0],' ', iter1,':', iter2,
                        ' ', iter1 + tempiter,':',iter2 + tempiter, sep='' )

def UpLeft(iter1, iter2, Word, Flag):
    if 0 <= iter1 - Word[3] < height and 0 <= iter2 - Word[3] < width:
        if grid[iter1 - Word[3]][iter2 - Word[3]] == Word[2]:
            tempiter = 1
            while tempiter < Word[3] and Flag == True:
                if grid[iter1 - tempiter][iter2 - tempiter] == Word[0][
                    tempiter]:
                    tempiter = tempiter + 1
                else:
                    Flag = False
            if Flag == True:
                print(Word[0],' ', iter1,':', iter2,' ', iter1 - tempiter,':',
                        iter2 - tempiter, sep='')

def DownLeft(iter1, iter2, Word, Flag):
    if 0 <= iter1 - Word[3] < height and 0 <= iter2 + Word[3] < width:
        if grid[iter1 - Word[3]][iter2 + Word[3]] == Word[2]:
            tempiter = 1
            while tempiter < Word[3] and Flag == True:
                if grid[iter1 - tempiter][iter2 + tempiter] == Word[0][
                    tempiter]:
                    tempiter = tempiter + 1
                else:
                    Flag = False
            if Flag == True:
                print(Word[0],' ', iter1,':', iter2,' ', iter1 - tempiter,':',
                        iter2 + tempiter, sep='')

def Down(iter1, iter2, Word, Flag):
    if 0 <= iter1 + Word[3] < height and 0 <= iter2 < width:
        if grid[iter1 + Word[3]][iter2] == Word[2]:
            tempiter = 1
            while tempiter < Word[3] and Flag == True:
                if grid[iter1 + tempiter][iter2] == Word[0][tempiter]:
                    tempiter = tempiter + 1
                else:
                    Flag = False
            if Flag == True:
                print(Word[0],' ', iter1,':', iter2,' ', iter1 + tempiter,':', iter2, sep='')

def UpRight(iter1, iter2, Word, Flag):
    if 0 <= iter1 - Word[3] < height and 0 <= iter2 + Word[3] < width:
        if grid[iter1 - Word[3]][iter2 + Word[3]] == Word[2]:
            tempiter = 1
            while tempiter < Word[3] and Flag == True:
                if grid[iter1 - tempiter][iter2 + tempiter] == Word[0][
                    tempiter]:
                    tempiter = tempiter + 1
                else:
                    Flag = False
            if Flag == True:
                print(Word[0],' ', iter1,':', iter2,' ', iter1 - tempiter,':',
                        iter2 + tempiter, sep='')

def Up(iter1, iter2, Word, Flag):
    if 0 <= iter1 - Word[3] < height and 0 <= iter2 < width:
        if grid[iter1 - Word[3]][iter2] == Word[2]:
            tempiter = 1
            while (tempiter < Word[3] and Flag == True):
                if grid[iter1 - tempiter][iter2] == Word[0][tempiter]:
                    tempiter = tempiter + 1
                else:
                    Flag = False
            if Flag == True:
                print(Word[0],' ', iter1,':', iter2,' ', iter1 - tempiter,':', iter2, sep='')

def Left(iter1, iter2, Word, Flag):
    if 0 <= iter1 < height and 0 <= iter2 - Word[3] < width:
        if grid[iter1][iter2 - Word[3]] == Word[2]:
            tempiter = 1
            while (tempiter < Word[3] and Flag == True):
                if grid[iter1][iter2 - tempiter] == Word[0][tempiter]:
                    tempiter = tempiter + 1
                else:
                    Flag = False
            if Flag == True:
                print(Word[0],' ', iter1,':', iter2,' ', iter1,':',
                        iter2 - tempiter, sep='')

def Right(iter1, iter2, Word, Flag):
    if 0 <= iter1 < height and 0 <= iter2 + Word[3] < width:
        if grid[iter1][iter2 + Word[3]] == Word[2]:
            tempiter = 1
            while (tempiter < Word[3] and Flag == True):
                if grid[iter1][iter2 + tempiter] == Word[0][tempiter]:
                    tempiter = tempiter + 1
                else:
                    Flag = False
            if Flag == True:
                print(Word[0],' ', iter1,':', iter2,' ', iter1,':', 
                        iter2 + tempiter, sep='')

# Removes extra spaces in word list
for i in range(wordcount):
    words[i] = words[i].strip()

# Gets first letter of each word
for i in range(wordcount):
    firstletters.append(words[i][0])

# Gets last letter of each word
for i in range(wordcount):
    lastletters.append(words[i][-1])

''' Creates a list of lists where each entry is a list with 4 elements the
first is the word itself
the second is the first letter
the third is the second letter
the fourth is the length of the word minus one to match up with the grid
location
'''
for i in range(wordcount):
    templist.append(words[i])
    templist.append(firstletters[i])
    templist.append(lastletters[i])
    templist.append((len(words[i]) - 1))
    Words.append(templist)
    templist = []

# cleans up the grid and stors it
for i in range(height):
    grid[i] = (grid[i].strip()).split(' ')

# closes the textfile
wordsearch.close()

# searches through the gird once and finds all the words we want
for i in range(height):
    for j in range(width):
        for l in range(wordcount):
            if grid[i][j] == Words[l][1]:
                Right(i, j, Words[l], Flag)
                DownRight(i, j, Words[l], Flag)
                UpRight(i, j, Words[l], Flag)
                Down(i, j, Words[l], Flag)
                UpLeft(i, j, Words[l], Flag)
                Up(i, j, Words[l], Flag)
                Left(i, j, Words[l], Flag)
                DownLeft(i, j, Words[l], Flag)
